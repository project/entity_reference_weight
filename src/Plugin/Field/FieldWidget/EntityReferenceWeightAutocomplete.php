<?php

namespace Drupal\entity_reference_weight\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Widget that uses autocomplete.
 *
 * @FieldWidget(
 *   id = "entity_reference_weight_autocomplete",
 *   label = @Translation("Autocomplete with weight"),
 *   description = @Translation("An autocomplete text field with weight."),
 *   field_types = {
 *     "entity_reference_weight"
 *   }
 * )
 */
class EntityReferenceWeightAutocomplete extends EntityReferenceAutocompleteWidget {

  const HIDE_WEIGHT = 'hide_weight';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + [self::HIDE_WEIGHT => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state) + [
      'hide_weight' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Hide weight property'),
        '#description' => $this->t('Hide the weight property and use the default value instead'),
        '#default_value' => $this->getSetting(self::HIDE_WEIGHT),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting(self::HIDE_WEIGHT)) {
      $summary[] = $this->t('Weight field is hidden');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = [
      '#attributes' => ['class' => ['form--inline', 'clearfix']],
      '#theme_wrappers' => ['container'],
    ];
    $widget['target_id'] = parent::formElement($items, $delta, $element, $form, $form_state);
    if ($this->getSetting(self::HIDE_WEIGHT)) {
      $widget['weight'] = [
        '#type' => 'value',
        '#value' => isset($items[$delta]->weight) ? $items[$delta]->weight : 0,
      ];
    }
    else {
      $widget['weight'] = [
        '#type' => 'number',
        '#size' => '4',
        '#default_value' => isset($items[$delta]->weight) ? $items[$delta]->weight : 0,
        '#weight' => 10,
      ];

      if ($this->fieldDefinition->getFieldStorageDefinition()->isMultiple()) {
        $widget['weight']['#placeholder'] = $this->fieldDefinition->getSetting('weight_label');
      }
      else {
        $widget['weight']['#title'] = $this->fieldDefinition->getSetting('weight_label');
      }
    }
    return $widget;
  }

}
