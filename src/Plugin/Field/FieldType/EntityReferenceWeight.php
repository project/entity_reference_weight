<?php

namespace Drupal\entity_reference_weight\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_reference_weight' field type.
 *
 * @FieldType(
 *   id = "entity_reference_weight",
 *   label = @Translation("Entity reference weight"),
 *   description = @Translation("An entity field containing an entity reference with weight property."),
 *   category = @Translation("Reference"),
 *   default_widget = "entity_reference_weight_autocomplete",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList"
 * )
 */
class EntityReferenceWeight extends EntityReferenceItem {

  /**
   * Defines the minimum weight of a child (but has the highest priority).
   */
  const WEIGHT_MIN_CHILD_WEIGHT = -100;

  /**
   * Defines the maximum weight of a child (but has the lowest priority).
   */
  const WEIGHT_MAX_CHILD_WEIGHT = 100;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $weight_definition = DataDefinition::create('integer')
      ->setLabel($field_definition->getSetting('weight_label'));
    $weight_definition->addConstraint('Range', ['min' => self::WEIGHT_MIN_CHILD_WEIGHT]);
    $weight_definition->addConstraint('Range', ['max' => self::WEIGHT_MAX_CHILD_WEIGHT]);
    $properties['weight'] = $weight_definition;
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['weight'] = [
      'type' => 'int',
      'unsigned' => FALSE,
    ];
    // Add weight index.
    $schema['indexes']['weight'] = ['weight'];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'weight_label' => t('Weight'),
      'weight_min' => self::WEIGHT_MIN_CHILD_WEIGHT,
      'weight_max' => self::WEIGHT_MAX_CHILD_WEIGHT,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::fieldSettingsForm($form, $form_state);

    $elements['weight_min'] = [
      '#type' => 'number',
      '#title' => t('Minimum'),
      '#default_value' => $this->getSetting('weight_min'),
    ];
    $elements['weight_max'] = [
      '#type' => 'number',
      '#title' => t('Maximum'),
      '#default_value' => $this->getSetting('weight_max'),
    ];
    $elements['weight_label'] = [
      '#type' => 'textfield',
      '#title' => t('Weight Label'),
      '#default_value' => $this->getSetting('weight_label'),
      '#description' => t('The weight of this node is relative to this entity reference.'),
    ];

    return $elements;
  }

}
